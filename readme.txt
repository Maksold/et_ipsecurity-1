== Extension permanent link
Permanent link: http://shop.etwebsolutions.com/eng/et-ip-security.html
Support link: http://support.etwebsolutions.com/projects/et-ipsecurity/issues/new

== Short Description
The extension gives you ability to restrict access to your website
by IP address or to close your shop for maintenance.

== Version Compatibility
Magento CE:
1.3.x (tested in 1.3.2.4)
1.4.x (tested in 1.4.2.0)
1.5.x (tested in 1.5.1.0)
1.6.x (tested in 1.6.1.0)
1.7.x (tested in 1.7.0.1)
1.8.x (tested in 1.8.0.0)
1.9.x (tested in 1.9.2.2)
1.9.x (tested in 1.9.3.1)

== Installation
http://doc.etwebsolutions.com/en/instruction/ip_security/install
