This file describes changes between versions of module ET_IpSecurity for Magento.

Legend:
* bug fix
+ added functionality
- removed functionality

TODO and some thoughts:
  + integrate anti spam service http://www.projecthoneypot.org/
  + add function: Access frontend only after authorization

=====================================
ver. 2.4.0 (20/03/2017)
+ added IPv6 support

ver. 2.3.0 (15/11/2016)
+ extension now use admin settings for access to admin orders rss

ver. 2.2.1 (11/10/2016)
* fixed undefined variable error while switching stores in IP Security configuration section
* fixed redirect to specified cms page on block for different stores,
  in backend it tries to redirect to website with same domain
* fixed Exception for IPv6 addresses, now IPv6 addresses bypassed

ver. 2.2.0 (20/06/2016)
+ added feature "Access by token",
  that allow to access website from blocked IP address if secret token is known and active.
* fixed undefined variable error while detecting IP by custom SERVER IP variable
* fixed error in log grid
* fixed potential vulnerability for bruteforce (via POST).
  The issue was not critical, because abuser was not able
  to receive the access to admin even on successful attack.

ver. 2.1.3 (27/11/2015)
* fixed: grid was not available for non admin users with permissions

ver. 2.1.2 (28/10/2015)
+ added support for Magento 1.9.2.2 and security patch SUPEE-6788

ver. 2.1.1 (30/04/2015)
+ added comments for some settings
+ extension can be installed with "Composer"
* fixed issue with usage of HTTP_X_FORWARDED_FOR (check only first IP)
  Example:
  HTTP_X_FORWARDED_FOR returns 192.168.1.100, 10.0.0.100
  in this case 192.168.1.100 is visitors IP that we should check


ver. 2.1.0 (13/10/2014)
+ added ability to select variable, where server stores IP address of current visitor.
  before only REMOTE_ADDR was used. now you can select
  HTTP_X_REAL_IP, HTTP_CLIENT_IP, HTTP_X_FORWARDED_FOR, HTTP_X_CLUSTER_CLIENT_IP
  depending on your server configuration
* fixed bug with wrong date in notification e-mails
* fixed bug in processing parameter "Email always" while blocking access to admin panel

ver. 2.0.1 (02/12/2013)
* fixed rare infinite redirects (sometimes trailing slash in redirect page may create infinite redirect)
* fixed typos and grammar

ver. 2.0.0 (10/09/2012)
+ license type changed from AFL to ETWS Free License v1 (EFL1)
+ code refactored for Magento standards
+ column "Last block rule" added to log table and grid
+ extension now blocks access to downloader page too (admin settings are used)
+ ability to use IP ranges (Example: 10.0.0.192/26|IP Range) added
+ some Unit tests written

ver. 1.5.3 (06/06/2011)
* minor bug fixes (variable initializing)

ver. 1.5.2
+ added ability to use IP ranges (Example: 10.0.0.1-20.0.0.1|IP Range)

ver. 1.5.1
* infinite loop resolved when admin block rule is triggered and option "Add Store Code to Urls" is set to Yes and redirect to CMS page is enabled

ver. 1.5.0
+ table for storing blocked ip's added to database
+ log grid added to admin panel
+ option added: send notification on every block or only once for each IP

ver. 1.2.0
+ code pool changed from local to community
+ settings section reworked
+ maintenance mode added

ver. 1.1.0
* helper added (without it Transactional Mails - Add New Template wasn't working) - relevant for Magento versions 1.4.х

ver. 1.0.9
+ ability to comment IP rules added
+ added notification by e-mail, if block rule is t